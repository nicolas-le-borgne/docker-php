# Docker php image
This php image is based on php:7.2.16-fpm and contains:

 - gd
 - imagick
 - intl
 - pdo_mysql 
 - pdo_pgsql 
 - zip
 - phpunit 6.5.14
 - composer 1.8.5
 - librdkafka
 - datadog php tracer
 - blackfire
 - Xdebug

